<?php

/**
 * @file
 * Main file.
 */

include_once "config.php";
include_once "autoloader.php";

/**
 * Connect to db and create tables.
 */
$pdo = Database::getInstance();

/**
 * All queries.
 */
$query = new QueryLibrary($pdo, $authors, $tables);

/**
 * TASK 1: Random fill DB.
 */
// $query->insertDefault();

/**
 * TASK 2:Update Author for the book by id.
 */
// $res = $query->updateAuthorForBook(1, 1, 'Новый Автор1');
// OR
// $id_author = $query->selectIdAuthorByName('Роджер Желязны');
// $id_book = $query->selectIdBookByTitle('Title15');
// $res = $query->updateAuthorFoBook($id_author, $id_book, 'Новый Автор2');

/**
 * TASK 3: Extract a list of books that are written by 3 co-authors.
 */
/*$res = $query->selectBookCountAuthors();
foreach($res as $value) {
  echo 'Title-' . $value['title'] . '. (' .  $value['COUNT(ab.id_author)'] . ')<br />';
}*/

/**
 * TASK 4: Write a request to delete a book written by a certain author.
 */
// $query->deleteBookByAuthor(2, 2);


/**
 *
 */
//$res = $query->insertBook('Title book', array('Author1', 'Author2', 'Author3'));
