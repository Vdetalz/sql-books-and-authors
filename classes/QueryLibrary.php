<?php

/**
 * @file
 * Queries.
 */

/**
 * Class QueryLibrary.
 */
class QueryLibrary {

  /**
   * PDO Connection.
   *
   * @var
   */
  protected $pdo;

  /**
   * Default Authors.
   *
   * @var
   */
  protected $defaultAuthors;

  /**
   * Default tables.
   *
   * @var
   */
  protected $defaultTables;

  /**
   * QueryLibrary constructor.
   *
   * @param resource $pdo
   * @param array $authors
   * @param array $tables
   */
  public function __construct($pdo, $authors, $tables) {
    $this->pdo            = $pdo;
    $this->defaultAuthors = $authors;
    $this->defaultTables  = $tables;
  }

  /**
   * Populating the database with default values.
   *
   * Use when is needed to fulling default random data into the db.
   */
  public function insertDefault() {

    $count          = 1;
    $defaultAuthors = $this->getDefaultAuthors();

    // Insert all authors.
    foreach ($defaultAuthors as $name) {
      // Insert.
      $authorInsert = $this->getPdo()
                           ->prepare("INSERT INTO author (name) VALUES (?)");
      $authorInsert->execute(array($name));
    }

    // Insert books and author_to_book.
    for ($id_book = 1; $id_book <= 50; $id_book++) {
      $randAuthId = array();
      // Random Authors.
      $randAuthId[] = $defaultAuthors[($count - 1)];
      $randAuthId[] = $defaultAuthors[($count)];
      $randAuthId[] = $defaultAuthors[($count + 1)];

      // Insert book.
      $titleInsert = $this->getPdo()
                          ->prepare("INSERT INTO book (title) VALUES (?)");
      $titleInsert->execute(array('Title' . $id_book));

      // Insert Authors and Select authors..
      for ($id_author = $count; $id_author < ($count + 3); $id_author++) {
        // Insert.
        $authorInsert = $this->getPdo()
                             ->prepare("INSERT INTO author_to_book (id_author, id_book) VALUES (:id_author, :id_book)");
        $authorInsert->execute(array(
          'id_author' => $id_author,
          'id_book'   => $id_book,
        ));
      }

      if (8 == $count) {
        $count = 1;
        continue;
      }
      $count++;
    }
  }

  /**
   * @param $title
   * @param $authors
   */
  public function insertBook($title, $authors) {
    $title = strip_tags(trim($title));

    if (!$this->selectIdBookByTitle($title)) {
      // Insert title into the table 'book'.
      $titleInsert = $this->getPdo()
                          ->prepare("INSERT INTO book (title) VALUES (?)");
      $titleInsert->execute(array($title));
      // Get the id of the book.
      $bookId = $this->selectIdBookByTitle($title);
      $id     = $bookId[0];

      // Insert authors.
      $newAuthors = array();
      foreach ($authors as $author) {
        $author = strip_tags(trim($author));

        if (!$this->selectIdAuthorByName($author)) {
          $authorInsert = $this->getPdo()
                               ->prepare("INSERT INTO author (name) VALUES (?)");
          $authorInsert->execute(array($author));
          // Get the id of the author.
          $authorId     = $this->selectIdAuthorByName($author);
          $newAuthors[] = $authorId[0];
        }
      }

      // Insert into author_to_book.
      foreach ($newAuthors as $newAuthor) {
        $a_t_b = $this->getPdo()->prepare("INSERT INTO author_to_book (id_author, id_book) VALUES(:id_author, :id_book)");
        $a_t_b->execute(array(':id_author' => $newAuthor, ':id_book' => $id));

      }
    }
    else {
      return FALSE;
    }
  }

  /**
   * Select book id by title.
   *
   * @param string $title
   *
   * @return bool
   */
  public function selectIdBookByTitle($title = NULL) {

    $title = mb_strtolower(strip_tags(trim($title)), 'utf-8');

    if (NULL == $title) {
      return FALSE;
    }

    $id = $this->getPdo()->prepare("SELECT id FROM book WHERE title=?");
    $id->bindValue(1, $title, PDO::PARAM_STR);
    $id->execute();
    $res = $id->fetchAll(PDO::FETCH_COLUMN);

    if (0 === count($res)) {
      return FALSE;
    }

    return $res;
  }

  /**
   * Select author id by name.
   *
   * @param string $name
   *
   * @return bool
   */
  public function selectIdAuthorByName($name = NULL) {

    $name = mb_strtolower(strip_tags(trim($name)), 'utf-8');

    if (NULL == $name) {
      return FALSE;
    }

    $id = $this->getPdo()->prepare("SELECT id FROM author WHERE name=?");
    $id->bindValue(1, $name, PDO::PARAM_STR);
    $id->execute();
    $res = $id->fetchAll(PDO::FETCH_COLUMN);

    if (0 === count($res)) {
      return FALSE;
    }

    return $res;
  }

  /**
   * Check existing ids.
   *
   * @param int $id
   * @param string $table
   *
   * @return bool
   */
  public function checkById($id, $table) {

    $table = mb_strtolower(strip_tags(trim($table)), 'utf-8');
    $id    = intval($id);

    if (in_array($table, $this->getDefaultTables()) && !empty($id)) {
      $smtp = $this->getPdo()->prepare("SELECT name FROM $table WHERE id=?");
      $smtp->bindValue(1, $id, PDO::PARAM_INT);
      $smtp->execute();
      $res = $smtp->fetchAll(PDO::FETCH_ASSOC);

      if (!empty($res)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Update Author for the book.
   *
   * @param int $id_book
   *   Id of the book.
   * @param int $id_author
   *   Id of the author.
   * @param string $author_new
   *   New author's name.
   *
   * @return string
   */
  public function updateAuthorForBook($id_book, $id_author, $author_new = NULL) {

    $id_author = intval(trim($id_author));
    $id_book   = intval(trim($id_book));
    $new_name  = strip_tags(trim($author_new));

    // Check existing auth.
    $check_author = $this->checkById($id_author, 'author');
    if (!$check_author) {
      return 'Error';
    }

    // Update author name.
    $update = $this->getPdo()
                   ->prepare("UPDATE author a INNER JOIN author_to_book ab ON a.id=ab.id_author SET a.name=:new_name WHERE ab.id_book=:id_book AND ab.id_author =:id_author AND a.id=:id_author");
    $update->bindParam(':id_author', $id_author);
    $update->bindParam(':id_book', $id_book);
    $update->bindParam(':new_name', $new_name);
    $update->execute();
  }

  /**
   * Extract a books that are written by 3 co-authors.
   *
   * @return mixed
   */
  public function selectBookCountAuthors() {

    // Select all books and count authors.
    $books = $this->getPdo()
                  ->prepare("SELECT b.title, COUNT(ab.id_author) FROM book b LEFT JOIN author_to_book ab ON ab.id_book=b.id WHERE b.id=ab.id_book GROUP BY b.title DESC HAVING COUNT(ab.id_author) > 2 ORDER BY b.title");
    $books->execute();
    $list = $books->fetchAll(PDO::FETCH_ASSOC);

    return $list;
  }

  /**
   * Delete all info about book that was written by author.
   *
   * @param int $id_author
   * @param int $id_book
   */
  public function deleteBookByAuthor($id_author, $id_book) {

    $id_author = intval(trim($id_author));
    $id_book   = intval(trim($id_book));

    $delete = $this->getPdo()
                   ->prepare("DELETE b.*, ab.*  FROM book b INNER JOIN author_to_book ab ON ab.id_book=b.id WHERE ab.id_book=:id_book AND ab.id_author=:id_author");
    $delete->bindParam(':id_book', $id_book);
    $delete->bindParam(':id_author', $id_author);
    $delete->execute();
  }

  /**
   * @return mixed
   */
  public function getDefaultAuthors() {
    return $this->defaultAuthors;
  }

  /**
   * @return mixed
   */
  public function getDefaultTables() {
    return $this->defaultTables;
  }

  /**
   * @return mixed
   */
  public function getPdo() {
    return $this->pdo;
  }

}
