<?php

/**
 * Class Database.
 */
class Database {
  /**
   * Database connection.
   * @var \PDO
   */
  protected static $pdo;

  /**
   * @var
   */
  private static $instance = NULL;

  /**
   * @return mixed
   */
  public static function getInstance() {

    if (NULL == self::$instance) {
      self::$instance = new self();
    }
    return self::$instance;
  }

  /**
   * Database constructor.
   */
  private function __construct() {
    Database::connect();
  }

  /**
   * @throws \Exception
   */
  public static function connect() {

    require __DIR__ . '/../config.php';
    $host = strip_tags(trim($host));
    $db   = strip_tags(trim($db));
    $user = strip_tags(trim($user));
    $pass = strip_tags(trim($pass));

    $connect = new PDO("mysql:host=$host;dbname=$db", $user, $pass);

    if ($connect) {
      self::$pdo = $connect;

      self::$pdo->exec("CREATE TABLE IF NOT EXISTS author (
                      id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      name VARCHAR(255) NOT NULL)
                      Engine=InnoDB DEFAULT CHARSET=utf8");
      self::$pdo->exec("CREATE TABLE IF NOT EXISTS book (
                      id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      title VARCHAR(255) NOT NULL)
                      Engine=InnoDB DEFAULT CHARSET=utf8");
      self::$pdo->exec("CREATE TABLE IF NOT EXISTS author_to_book (
                      id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                      id_author int(11) NOT NULL,
                      id_book int(11) NOT NULL)
                      Engine=InnoDB DEFAULT CHARSET=utf8");
      self::$pdo->exec("ALTER TABLE author_to_book
                        ADD KEY id_author (`id_author`),
                        ADD KEY id_book (id_book)");
    }
    else {
      throw new Exception(mysql_error());
    }
  }

  private function __clone() {
  }

  private function __wakeup() {
  }

}
