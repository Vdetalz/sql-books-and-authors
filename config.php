<?php

/**
 * @file
 * Config data.
 */

/**
 * DB config.
 */
$host = 'localhost';
$user = 'root';
$pass = 'root';
$db   = 'library';

/**
 * Authors default config.
 */
$authors = [
  'Стивен Кинг',
  'Роджер Желязны',
  'Джордж Мартин',
  'Терри Пратчетт',
  'Рей Брэдбэри',
  'Анджей Спаковский',
  'Терри Гудкайнд',
  'Скотт Брэккер',
  'Андрэ Нортон',
  'Роман Злотников',
];

/**
 * Tables config.
 */
$tables = [
  'author',
  'books',
  'author_to_books',
];
